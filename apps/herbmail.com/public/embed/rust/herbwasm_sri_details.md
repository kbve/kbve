---
js_integrity: sha384-6RtZGP_ubyilCgFK4FExOiQWTQJPC15urf0ievF2kTUyyT6Hi615C1218wPM2Uce
js_file: /rust_wasm_embed-8fbdd6183552229d.js
wasm_integrity: sha384-cTDesVjzuMP6FC9KSoWYVFYuM7B6orIr0wtGZEpoz6BNePLjBqZ4jmoCk-usWtGK
wasm_file: /rust_wasm_embed-8fbdd6183552229d_bg.wasm
---
